package org.example.controllers;

import org.example.entities.Speciality;
import org.example.repositories.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/specialities")
public class SpecialityController {

    private SpecialityRepository specialityRepository;

    public SpecialityRepository getSpecialityRepository() {
        return specialityRepository;
    }

    @Autowired
    public void setSpecialityRepository(SpecialityRepository specialityRepository) {
        this.specialityRepository = specialityRepository;
    }

    @GetMapping("/")
    public ResponseEntity<List<Speciality>> getAll(){
        final List<Speciality> specialities = getSpecialityRepository().findAll();

        return specialities != null &&  !specialities.isEmpty()
                ? new ResponseEntity<>(specialities, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        final Speciality speciality = getSpecialityRepository().getOne(id);

        return speciality != null
                ? new ResponseEntity<>(speciality, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    public ResponseEntity<?> add(@RequestBody Speciality speciality){
        getSpecialityRepository().save(speciality);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/")
    public ResponseEntity<?> update(@RequestBody Speciality speciality) {
        getSpecialityRepository().save(speciality);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<?> delete() {
        getSpecialityRepository().deleteAll();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        getSpecialityRepository().deleteById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
