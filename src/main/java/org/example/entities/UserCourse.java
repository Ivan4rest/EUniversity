package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_courses")
public class UserCourse {
    @EmbeddedId
    private UserCourseKey id;

    @ManyToOne(cascade = CascadeType.ALL,
                fetch = FetchType.EAGER)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL,
                fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    @MapsId("courseId")
    private Course course;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("departmentId")
    @JoinColumn(name = "department_id")
    private Department department;

    public UserCourse() {
    }

    public UserCourse(User user, Course course, Department department) {
        this.user = user;
        this.course = course;
        this.department = department;
    }

    public UserCourseKey getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
