package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_groups")
public class UserGroup {
    @EmbeddedId
    private UserGroupKey id;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("groupId")
    @JoinColumn(name = "group_id")
    private Group group;

    public UserGroup() {
    }

    public UserGroup(User user, Group group) {
        this.user = user;
        this.group = group;
    }

    public UserGroupKey getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
