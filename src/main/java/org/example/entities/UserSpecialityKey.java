package org.example.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserSpecialityKey implements Serializable {

    @Column(name = "user_id")
    Long userId;

    @Column(name = "speciality_id")
    Long specialityId;

    @Column(name = "course_id")
    Long courseId;

}
