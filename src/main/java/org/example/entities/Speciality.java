package org.example.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "specialities")
public class Speciality {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    private String name;

    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "speciality")
    private Set<UserSpeciality> userSpecialities;

    public Speciality() {
    }

    public Speciality(String name, Department department) {
        this.name = name;
        this.department = department;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<UserSpeciality> getUserSpecialities() {
        return userSpecialities;
    }

    public void setUserSpecialities(Set<UserSpeciality> userSpecialities) {
        this.userSpecialities = userSpecialities;
    }
}
