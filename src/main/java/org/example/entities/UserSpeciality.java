package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_specialities")
public class UserSpeciality {
    @EmbeddedId
    private UserSpecialityKey id;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("specialityId")
    @JoinColumn(name = "speciality_id")
    private Speciality speciality;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    private Course course;

    public UserSpeciality() {
    }

    public UserSpeciality(User user, Speciality speciality, Course course) {
        this.user = user;
        this.speciality = speciality;
        this.course = course;
    }

    public UserSpecialityKey getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
